package com.airhacks.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import com.airhacks.entity.Book;

@Stateless
public class BookService {
	
	private List<Book> buildBooks(){
		List<Book> books = new ArrayList<>();
		Book book = new Book(1, "Java ee 7", 1.2f, "Beginning Java EE 7", "Adam Bien");
		Book book2 = new Book(2, "Jakarta 8", 2.2f, "Beginning Jakarta 8", "Jonh");
		Book book3 = new Book(3, "Microprofile 3.0", 3.2f, "Microprofile 3.0", "David");
		books.add(book);
		books.add(book2);
		books.add(book3);
		return books;
	}
	
	
	public List<Book> getAll(){
		return buildBooks();
	}

}
