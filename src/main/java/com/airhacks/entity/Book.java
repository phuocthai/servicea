package com.airhacks.entity;

public class Book {

	private Integer id;
	private String title;
	private Float price;
	private String description;
	private String author;

	public Book() {
	}

	public Book(Integer id, String title, Float price, String description, String author) {
		this.id = id;
		this.title = title;
		this.price = price;
		this.description = description;
		this.author = author;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

}
