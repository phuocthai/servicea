# Build
mvn clean package && docker build -t com.microprofile/servicea .

# RUN

docker rm -f servicea || true && docker run -d -p 8080:8080 -p 4848:4848 --name servicea com.microprofile/servicea 